var mook = {
    sobn: function () {
        let raw = `
        Bệnh viện Nhi Đồng Thành Phố Cần Thơ
        Bệnh viện Đa Khoa Thành Phố Cần Thơ
        Bệnh viện Đa Khoa Quận Ô Môn
        Bệnh viện Đa Khoa  Quận Thốt Nốt
        Bệnh viện Đa Khoa Huyện Vĩnh Thạnh
        Bệnh viện Y học cổ truyền
        Bệnh viện Mắt - Răng Hàm Mặt`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function () {
        let raw = `
        Bệnh viện Nhi Đồng Thành Phố Cần Thơ
        Bệnh viện Đa Khoa Thành Phố Cần Thơ
        Bệnh viện Đa Khoa Quận Ô Môn
        Bệnh viện Đa Khoa  Quận Thốt Nốt
        Bệnh viện Đa Khoa Huyện Vĩnh Thạnh
        Bệnh viện Y học cổ truyền
        Bệnh viện Mắt - Răng Hàm Mặt`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function () {
        let raw = `Q. Bình Thủy
                Q. Cái Răng‎
                H. Bắc Tân Uyên‎
                Q. Ninh Kiều
                Q. Ô Môn‎‎
                H. Phong Điền
                Q. Thốt Nốt
                H. Thới Lai‎‎
                H. Vĩnh Thạnh`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },
    cdv: function () {
        let raw = `
        Bệnh viện Nhi Đồng Thành Phố Cần Thơ
        Bệnh viện Đa Khoa Thành Phố Cần Thơ
        Bệnh viện Đa Khoa Quận Ô Môn
        Bệnh viện Đa Khoa  Quận Thốt Nốt
        Bệnh viện Đa Khoa Huyện Vĩnh Thạnh
        Bệnh viện Y học cổ truyền
        Bệnh viện Mắt - Răng Hàm Mặt`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    http: function () {
        let raw = `
        Bệnh viện Nhi Đồng Thành Phố Cần Thơ
        Bệnh viện Đa Khoa Thành Phố Cần Thơ
        Bệnh viện Đa Khoa Quận Ô Môn
        Bệnh viện Đa Khoa  Quận Thốt Nốt
        Bệnh viện Đa Khoa Huyện Vĩnh Thạnh
        Bệnh viện Y học cổ truyền
        Bệnh viện Mắt - Răng Hàm Mặt`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    }
}
