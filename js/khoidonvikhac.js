var datakhoidonvikhac = [
 {
   "STT": 1,
   "Name": "Chi cục Dân số - Kế hoạch hóa gia đình",
   "address": "320 đường 30/4, phường Hưng Lợi, quận Ninh Kiều, TP. Cần Thơ",
   "Longtitude": 10.0120517,
   "Latitude": 105.7599873
 },
 {
   "STT": 2,
   "Name": "Trung tâm Truyền thông Giáo dục sức khỏe",
   "address": "234 Trần Hưng Đạo, phường An Nghiệp, quận Ninh Kiều, TP Cần Thơ",
   "Longtitude": 10.033647,
   "Latitude": 105.775018
 },
 {
   "STT": 3,
   "Name": "Trung tâm Pháp Y",
   "address": "69 Phan Đăng Lưu, phường Thới Bình, quận Ninh Kiều",
   "Longtitude": 10.045382,
   "Latitude": 105.77931
 },
 {
   "STT": 4,
   "Name": "Trung tâm Y tế dự phòng thành phố",
   "address": "01 đường Ngô Đức Kế, phường An Lạc, quận Ninh Kiều, TP Cần Thơ",
   "Longtitude": 10.0301083,
   "Latitude": 105.7854827
 },
 {
   "STT": 5,
   "Name": "Ban Bảo vệ CSSK cán bộ Thành uỷ",
   "address": "quận Ninh Kiều, TP Cần Thơ",
   "Longtitude": 10.0272537,
   "Latitude": 105.7698039
 },
 {
   "STT": 6,
   "Name": "Trung tâm Kiểm nghiệm Thuốc - Mỹ phẩm - Thực phẩm",
   "address": "399/9 Nguyễn Văn Cừ, Phường An Bình, quận Ninh Kiều, TP Cần Thơ",
   "Longtitude": 10.0313873,
   "Latitude": 105.7524717
 },
 {
   "STT": 7,
   "Name": "Trung tâm Giám định Y khoa",
   "address": "52 Nguyễn An Ninh, phường Tân An, quận Ninh Kiều, TP Cần Thơ",
   "Longtitude": 10.0311873,
   "Latitude": 105.7862056
 },
 {
   "STT": 8,
   "Name": "Bệnh viện Đa Khoa Thành Phố Cần Thơ",
   "address": "69 đường Hùng Vương, phường Thới Bình, quận Ninh Kiều, TP Cần Thơ",
   "Longtitude": 10.042234,
   "Latitude": 105.7783229
 },
 {
   "STT": 9,
   "Name": "Trung tâm Bảo vệ Sức khỏe lao động và Môi trường",
   "address": "154 Nguyễn An Ninh, phường Tân An  , quận Ninh Kiều, TP Cần Thơ",
   "Longtitude": 10.0315581,
   "Latitude": 105.7834615
 },
 {
   "STT": 10,
   "Name": "Trung tâm Phòng chống  HIV/AIDS",
   "address": "Đường Nguyễn Văn Cừ Nối dài, quận Ninh Kiều, TP.Cần Thơ",
   "Longtitude": 10.0144943,
   "Latitude": 105.7349012
 },
 {
   "STT": 11,
   "Name": "Chi cục An toàn vệ sinh thực phẩm",
   "address": "12 Ngô Hữu Hạnh, P. An Hội, quận Ninh Kiều, Tp. Cần Thơ",
   "Longtitude": 10.0372561,
   "Latitude": 105.7858948
 }
];