﻿$(function () {
    var chart = new Highcharts.chart('container', {
        data: {
            table: 'datatable'
        },
        chart: {
            type: 'column',
            //events: {
            //    load: function () {
            //        var count = 0;
            //        setInterval(function () {
            //            if (count == 0) {
            //                chart.series[0].setData([86, 95, 82, 85]);
            //                chart.series[1].setData([14, 5, 18, 15]);
            //                //chart.series[1].setData([10.57]);
            //                //chart.series[2].setData([7.23]);
            //                count = 1;
            //            }
            //            else {
            //                chart.series[0].setData([0, 0, 0, 0]);
            //                chart.series[1].setData([0, 0, 0, 0]);
            //                //chart.series[1].setData([0]);
            //                //chart.series[2].setData([0]);
            //                count = 0;
            //            }
            //        }, 2000);
            //    }
            //}
        },
        title: {
            text: 'KẾT QUẢ GIẢI QUYẾT PHẢN ÁNH CỦA NGƯỜI DÂN',
            style: {
                color: '#000000',
                fontWeight: 'bold'
            }
        },
        yAxis: {
            max:100,
            allowDecimals: false,
            title: {
                text: 'Tỷ lệ %'
            }
        },
        colors: ['blue', 'orange'],
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                  this.point.y + ' ' + this.point.name.toLowerCase();
            }
        },
        credits: {
            enabled: false
        },
        exporting: { enabled: false }
    });
});